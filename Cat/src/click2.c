#include <stdio.h>
#include <string.h>

int main()
{
  char s[100];
  strcpy(s, "Fred");
  strcpy(s+5, "Waluigi");
  memcpy(s+3, "Mario", 3);
  printf("%d\n", (int) (strchr(s, 'i') - s));
  return 0;
}
