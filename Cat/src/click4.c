#include <stdio.h>
#include <string.h>
#include <unistd.h>

int main()
{
  char b[30];
  int i;

  strcpy(b, "ABCDEFGHIJKLMNOPRSTUVWXYZ");
  i = ('C' << 16) | ('D' << 8) | 'E';
  memcpy(b+2, &i, 4);
  printf("%d\n", (int) strlen(b));
  return 0;
}
