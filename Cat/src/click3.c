#include <stdio.h>
#include <string.h>
#include <unistd.h>

/* Line  1 */  int main()
/* Line  2 */  {
/* Line  3 */    char s[100];
/* Line  4 */    int *vec;
/* Line  5 */    void *v[100];
/* Line  6 */    void *x[100];
/* Line  7 */    int i;
/* Line  8 */  
/* Line  9 */    strcpy(s, "");
/* Line 10 */    for (i = 0; i < 50; i++) {
/* Line 11 */      strcat(s, "j");
/* Line 12 */      vec[i] = i;
/* Line 13 */      v[i] = (void *) i;
/* Line 14 */      x[i] = (void *) &i;
/* Line 15 */    }
/* Line 16 */    return 0;
/* Line 17 */  }
