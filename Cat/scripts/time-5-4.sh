echo "--------------------------------"
echo "Timing simpcat5.c (fread/fwrite)"
echo ""

bsize=1048576

while [ $bsize != 0 ]; do
  s=`( time sh -c "bin/simpcat5 $bsize < data/large.txt > /dev/null" ) 2>&1`
  echo $bsize $s | awk '{ printf "%9d %s\n", $1, $3 }'
  bsize=$(($bsize/2))
done

echo "--------------------------------"
echo "Timing simpcat4.c (read/write)"
echo ""

bsize=1048576

while [ $bsize != 0 ]; do
  s=`( time sh -c "bin/simpcat4 $bsize < data/large.txt > /dev/null" ) 2>&1`
  echo $bsize $s | awk '{ printf "%9d %s\n", $1, $3 }'
  bsize=$(($bsize/2))
done

