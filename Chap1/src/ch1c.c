/* ch1c.c
   James S. Plank */

/* This program prints out a counter that increments every second.
   You stop it temporarily by typing <CNTL-Z>, which sends the SIGSTOP signal to
   the program.  You can run it again by typing fg.  Then, you can terminate the
   program with <CNTL-C>, which sends it the SIGINT signal. */

#include <stdio.h>
#include <unistd.h>

int main()
{
  int i;

  i = 0;
  while (1) {
    i++;
    printf("%d\n", i);
    fflush(stdout);
    sleep(1);
  }
  return 0;
}
