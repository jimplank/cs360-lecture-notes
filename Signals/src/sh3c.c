/* We're trying to outsmart the compiler.  Instead of having
   our loop do nothing, we'll have it repeatedly do a small calculation.
   We're hoping that the compiler can't figure this out and delete the loop! */

#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

volatile int i, j, seconds;

void alarm_handler(int dummy)
{
  seconds++;
  printf("%d second%s just passed: j = %4d.  i = %6d\n", seconds,
     (seconds == 1) ? " " : "s", j, i);
  signal(SIGALRM, alarm_handler);
  alarm(1);
}

int main()
{
  int k;
  seconds = 0;

  signal(SIGALRM, alarm_handler);
  alarm(1);

  k = 0;
  for (j = 0; j < 5000; j++) {
    for (i = 0; i < 1000000; i++) k = (k < 0) ? (i -j) : (j - i);
  }
  printf("%d %d %d\n", j, i, k);
  return 0;
}
