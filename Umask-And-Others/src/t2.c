/* This program shows how you can use the (struct tm *) to manipulate time. */

#include <time.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{
  time_t years[10];        /* Now, and every year up to 9 years from now */
  struct tm *tm1;    
  int i;

  /* Set years[0] to now, convert to tm1 and print. */

  years[0] = time(0);          
  tm1 = localtime(&years[0]);
  printf("Base time: %ld. %14s  %s", years[0], "", asctime(tm1));

  /* Now use the (struct tm *) to increment the years, and print.
     When you run this, you'll see that leap years are handled correctly. */

  for (i = 1; i < 10; i++) {
    tm1->tm_year += 1;
    years[i] = mktime(tm1);
    printf("Year +%d:   %ld. Diff: %ld. %s", i, years[i], years[i]-years[i-1], asctime(tm1));
  }

  return 0;
}
