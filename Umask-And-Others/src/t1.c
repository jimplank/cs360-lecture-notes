/* This program shows time(), localtime() and asctime(). */

#include <time.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{
  time_t now;
  struct tm *tm1, *tm2;

  /* Set now to the current time, 
     use localtime() to convert it to a (struct tm *),
     and print them both out. */

  now = time(0);

  tm1 = localtime(&now);
  printf("Seconds: %ld.  Asctime(tm1): %s\n", now, asctime(tm1));

  /* Add an hour to "now" and do the same, using
     tm2 for the (struct tm *). */

  now += 3600;
  tm2 = localtime(&now);
  printf("Seconds: %ld.  Asctime(tm2): %s\n", now, asctime(tm2));

  /* Print out tm1.  Is that what you expect? */

  printf("Asctime(tm1): %s\n", asctime(tm1));

  printf("0x%lx 0x%lx\n", (long unsigned int) tm1, (long unsigned int) tm2); 
  return 0;
}
