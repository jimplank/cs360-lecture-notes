#/bin/sh

# Note to self -- I've been having problems with this -- just put the commands into a
# text file and run it.

if [ $# -ne 4 ]; then
  echo 'usage: sh kill_it.sh host port nentries nclients' >&2
  exit 1
fi

i=0
while [ $i -lt $4 ]; do
  echo "bin/inclient $1 $2 $3 $i lns.txt &"
  bin/inclient $1 $2 $3 $i lns.txt > /dev/null &
  i=$(($i+1))
done

i=0
while [ $i -lt $4 ]; do
  wait
  i=$(($i+1))
done
