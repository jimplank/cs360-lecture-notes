/* This prints the modification times of the files on the command line, in seconds. */

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

int main(int argc, char **argv)
{
  int i;
  struct stat buf;

  for (i = 1; i < argc; i++) {
    if (stat(argv[i], &buf) != 0) {
      printf("Couldn't stat %s\n", argv[i]);
    } else {
      printf("%s %ld\n", argv[i], buf.st_mtime);
    }
  }
  return 0;
}
