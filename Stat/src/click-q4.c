#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
  char s[60];

  s[0] = 'm';
  s[1] = '\0';
  s[2] = 'x';
  s[3] = 'y';
  s[4] = 'z';

  strcat(s, "ab");
  printf("%s\n", s);
  return 0;
}
