/* o1.c
   James S. Plank
   September, 1996.
   Latest revision: Wed Feb  3 14:38:29 EST 2021
 */

/* This program opens the file "txt/in1.txt" in the current directory, and prints out the
   return value of the open() system call.  If "txt/in1.txt" exists, open() will return a
   non-negative integer (three).  If "txt/in1.txt" does not exist, then it will return -1. */

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{
  int fd;

  fd = open("txt/in1.txt", O_RDONLY);
  printf("%d\n", fd);
  return 0;
}
