#-Philosophers: 9
  0 Philosopher 0 Thinking (2)
  0 Philosopher 3 Thinking (1)
  0 Philosopher 7 Thinking (3)
  0 Philosopher 1 Thinking (3)
  0 Philosopher 5 Thinking (5)
  0 Philosopher 2 Thinking (4)
  0 Philosopher 6 Thinking (3)
  0 Philosopher 4 Thinking (1)
  0 Philosopher 8 Thinking (2)
  1 Philosopher 3 Hungry
  1 Philosopher 3 Picked Up Stick 3
  1 Philosopher 3 Picked Up Stick 4
  1 Philosopher 3 Eating (1)
  1 Philosopher 4 Hungry
  1 Philosopher 4 Blocking on Stick 4
  2 Philosopher 0 Hungry
  2 Philosopher 0 Picked Up Stick 0
  2 Philosopher 0 Picked Up Stick 1
  2 Philosopher 0 Eating (3)
  2 Philosopher 8 Hungry
  2 Philosopher 8 Picked Up Stick 8
  2 Philosopher 8 Blocking on Stick 0
  2 Philosopher 3 Put Down Stick 4
  2 Philosopher 3 Put Down Stick 3
  2 Philosopher 3 Thinking (4)
  2 Philosopher 4 Picked Up Stick 4
  2 Philosopher 4 Picked Up Stick 5
  2 Philosopher 4 Eating (1)
  3 Philosopher 7 Hungry
  3 Philosopher 1 Hungry
  3 Philosopher 6 Hungry
  3 Philosopher 7 Picked Up Stick 7
  3 Philosopher 7 Blocking on Stick 8
  3 Philosopher 1 Blocking on Stick 1
  3 Philosopher 6 Picked Up Stick 6
  3 Philosopher 6 Blocking on Stick 7
  3 Philosopher 4 Put Down Stick 5
  3 Philosopher 4 Put Down Stick 4
  3 Philosopher 4 Thinking (2)
  4 Philosopher 2 Hungry
  4 Philosopher 2 Picked Up Stick 2
  4 Philosopher 2 Picked Up Stick 3
  4 Philosopher 2 Eating (2)
  5 Philosopher 5 Hungry
  5 Philosopher 0 Put Down Stick 1
  5 Philosopher 0 Put Down Stick 0
  5 Philosopher 5 Picked Up Stick 5
  5 Philosopher 5 Blocking on Stick 6
  5 Philosopher 0 Thinking (2)
  5 Philosopher 8 Picked Up Stick 0
  5 Philosopher 8 Eating (3)
  5 Philosopher 1 Picked Up Stick 1
  5 Philosopher 1 Blocking on Stick 2
  5 Philosopher 4 Hungry
  5 Philosopher 4 Picked Up Stick 4
  5 Philosopher 4 Blocking on Stick 5
  6 Philosopher 2 Put Down Stick 3
  6 Philosopher 2 Put Down Stick 2
  6 Philosopher 2 Thinking (3)
  6 Philosopher 1 Picked Up Stick 2
  6 Philosopher 1 Eating (2)
  6 Philosopher 3 Hungry
  6 Philosopher 3 Picked Up Stick 3
  6 Philosopher 3 Blocking on Stick 4
  7 Philosopher 0 Hungry
  7 Philosopher 0 Blocking on Stick 0
  8 Philosopher 8 Put Down Stick 0
  8 Philosopher 8 Put Down Stick 8
  8 Philosopher 8 Thinking (1)
  8 Philosopher 0 Picked Up Stick 0
  8 Philosopher 0 Blocking on Stick 1
  8 Philosopher 7 Picked Up Stick 8
  8 Philosopher 7 Eating (5)
  8 Philosopher 1 Put Down Stick 2
  8 Philosopher 1 Put Down Stick 1
  8 Philosopher 1 Thinking (2)
  8 Philosopher 0 Picked Up Stick 1
  8 Philosopher 0 Eating (4)
  9 Philosopher 8 Hungry
  9 Philosopher 8 Blocking on Stick 8
  9 Philosopher 2 Hungry
  9 Philosopher 2 Picked Up Stick 2
  9 Philosopher 2 Blocking on Stick 3
 10 Philosopher 1 Hungry
 10 Philosopher 1 Blocking on Stick 1
 12 Philosopher 0 Put Down Stick 1
 12 Philosopher 0 Put Down Stick 0
 12 Philosopher 0 Thinking (3)
 12 Philosopher 1 Picked Up Stick 1
 12 Philosopher 1 Blocking on Stick 2
 13 Philosopher 7 Put Down Stick 8
 13 Philosopher 7 Put Down Stick 7
 13 Philosopher 7 Thinking (5)
 13 Philosopher 8 Picked Up Stick 8
 13 Philosopher 8 Picked Up Stick 0
 13 Philosopher 8 Eating (2)
 13 Philosopher 6 Picked Up Stick 7
 13 Philosopher 6 Eating (2)
 15 Philosopher 8 Put Down Stick 0
