#!/bin/sh

if [ $# -ne 1 ]; then
  echo "usage: sh scripts/pull_timings.sh 1-6" >&2
  exit 1
fi

n=$(($1*4))

egrep '500 20000000|Total-Hungry|Individual-Hungry|CNTL-C' lecture.html | head -n $n |
      sed 's/.*bin\/\([^ ]*\) .*/------------ Program: \1/' |
      sed 's/.*CNTL-C.*//'
