#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct ms {
   char a;
   char b;
   char c;
   char d;
   int e;
   char *f;
   struct ms *g;
} Mystruct;

char buf[100];
char *ctos(char b)
{
  if (b == 0) {sprintf(buf, "'\\0'"); return buf; }
  if (b == '\n') {sprintf(buf, "'\\n'"); return buf; }
  if (b <= 'Z' && b >= 'A') {sprintf(buf, "'%c'", b); return buf; }
  if (b <= 'z' && b >= 'a') {sprintf(buf, "'%c'", b); return buf; }
  if (b <= '9' && b >= '0') {sprintf(buf, "'%c'", b); return buf; }
  if (strchr(".,></?;:[{]}|=+-_)(*&^%$#@!'`~", b) != NULL) {sprintf(buf, "'%c'", b); return buf; }
  sprintf(buf, " -- ");
  return buf;
}

void  printt4(int i)
{
  char *s;

  s = (char *) &i;

  printf("%-4s|", ctos(s[0]));
  printf("%-4s|", ctos(s[1]));
  printf("%-4s|", ctos(s[2]));
  printf("%-4s", ctos(s[3]));
}

void printmemory(char *address, int nn)
{
  int i;
  char c[30];
  int *b;
  double d;

  b = (int *) address;
  for (i = 0; i < nn; i++) {
    sprintf(c, "0x%x", b[i]);
    memcpy(&d, b+i, 8);
    if (i % 2 == 0) {
      printf("0x%x %13d %13s    ", (unsigned int) (b+i), b[i], c);
    } else {
      printf("0x%x %13d %13s    ", (unsigned int) (b+i), b[i], c);
    }
    printt4(b[i]);
    printf("\n");
  }
}

typedef unsigned long UL;

void pm(Mystruct *p)
{
   Mystruct *q;

   printf("%c\n", p->b);
   printf("%c\n", p->f[0]);
   printf("%d\n", p[1].e);
   q = p+4;
   printf("0x%x\n", (unsigned int) q->f);
   q = p->g;
   printf("%c\n", (unsigned int) q->a);
   q = q[1].g;
   printf("%c\n", (unsigned int) q->a);
}

int main()
{
  int i, j;

  void *a;
  char *ac;
  size_t len;

  len = 256*256*256;

  srand(50);

  a = mmap((void *) 0x61610000, len, PROT_READ | PROT_WRITE, MAP_ANON | MAP_FIXED | MAP_PRIVATE, -1, 0);
  if (a == MAP_FAILED) {
    perror("mmap");
    exit(1);
  }

  ac = (char *) 0x61626300;
  j = 0;
  for (i = 36; i <= 124; i += 4) {
    if (j == 3) {
      ac[i] = (rand()%(128-36)+36) & (~3); 
    } else {
      ac[i] = (rand()%(128-36)+36);
    }
    ac[i+1] = 'c';
    ac[i+2] = 'b';
    ac[i+3] = 'a';
    j++;
    j %= 4;
  };

  printmemory(ac+36, (128-36)/4);
  printf("\n");

  pm((Mystruct *) (ac+36));
}
