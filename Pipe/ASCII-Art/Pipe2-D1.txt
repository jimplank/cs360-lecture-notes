pipe1(parent)                                             pipe1(child)
|----------|      file-                                   |----------|
| code,    |   descriptors                                | code,    |
| globals, |                  |---------|                 | globals, |
| heap.    |       0 <-----   |         |  -----> 0       | heap.    |
|          |       1 ----->   |operating|  <----- 1       |          |
|          |       2 ----->   | system  |  <----- 2       |          |
|          | pipefd[0] <---   |         |  ---> pipefd[0] |          |
|          | pipefd[1] --->   |---------|  <--- pipefd[1] |          |
|          |                                              |          |
| stack    |                                              | stack    |
|----------|                                              |----------|

1,13s/............................/                            /
