#include <stdio.h>
#include <stdlib.h>

int main()
{
  int *p;
  unsigned char *q;
  int *r;
  int i;

  p = (int *) malloc(sizeof(int) * 4);      // Allocate 16 bytes for p and fill some of them in
  p[0] = 678;
  p[1] = 0x6824abcd;
  p[3] = 5555;

  q = (unsigned char *) malloc(sizeof(unsigned char) * 6);   // Now 6 bytes for q
  q[2] = 0x23;
  q[7] = 0xfe;              // This writes past the "allocated" region but doesn't harm anything

  free(p);                  // free p to see its memory chunk go onto the free list

  p = (int *) malloc(sizeof(int) * 6);      // Allocate 24 bytes and fill them in
  for (i = 0; i < 6; i++) p[i] = 100+i;

  r = (int *) malloc(sizeof(int) * 2);      // Allocate 8 bytes and fill in 12 bytes past what's allocated
  for (i = 0; i < 5; i++) r[i] = 200+i;
  
  q[8] = 127;                               // This is going to damage the next allocated chunk
  return 0;
}
