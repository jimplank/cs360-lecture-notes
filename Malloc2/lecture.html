<title>CS360 Lecture notes -- Malloc Lecture #2</title>
<body bgcolor=ffffff>
<h1>CS360 Lecture notes -- Malloc Lecture #2</h1>
<LI><a href=http://web.eecs.utk.edu/~jplank>James S. Plank</a>
<LI>Directory: <b>/home/plank/cs360/notes/Malloc2</b>
<LI>Lecture notes:
    <a href=http://web.eecs.utk.edu/~jplank/plank/classes/cs360/360/notes/Malloc2/lecture.html>
    <b>
  http://web.eecs.utk.edu/~jplank/plank/classes/cs360/360/notes/Malloc2/lecture.html
</b></a>
<LI> Original Notes: 1996.
<LI> Last updated:
Mon Mar 28 17:31:50 EDT 2022
<LI> <a href=Malloc-Marz-2/index.html> The material Stephen Marz used when he taught
this lecture in 2017.</a>
<hr>

<h3>More on malloc() and free().</h3>

Last class I discussed how malloc keeps a
big buffer of memory in the heap and carves it up
and doles it out whenever the user calls
<b>malloc()</b>.  If there is not enough room in the
buffer for what the user desires, then <b>sbrk()</b> is
called to get heap storage for a big enough
buffer. 

<p><b>Malloc</b> actually works in a different way.  What
it really does is maintain a linked list of free
memory.  When <b>malloc()</b> is called, it looks on its
list for a piece of memory that is big enough.
If it finds one, then it removes that memory from
the linked list and returns it to the user.  When
<b>free()</b> is called, the memory is put back on the
linked list.  Now, to be efficient, if there is a
chunk of memory on the free list that much bigger
than what is requested, then it breaks up that
chunk into two chunks -- one which is the size of
the request (padded to a multiple of 8),
and the remainder.  The remainder is put on the
free list and the one the size of the request is
returned to the user.  This is the standard way
that you view <b>malloc</b> -- it manages a free list of
memory.  <b>Malloc()</b> takes memory from the free list
and gives it to the user, and <b>free()</b> puts memory
back to the free list. 

<p>Initially, the free list is empty.  When the
first <b>malloc()</b> is called, we call <b>sbrk()</b> to get a
new chunk of memory for the free list.  This
memory is split up so that some is returned to
the user, and the rest goes back onto the free
list. 

<p>Before going into an example, I should say
something about how the free list is implemented.
There will be a global variable <b>malloc_head</b>,
which is the head of the free list.  Initially,
<b>malloc_head</b> is <b>NULL</b>.  When <b>malloc()</b> is first
called, <b>sbrk()</b> is called so that some memory can
be put on the free list.  The way to turn
memory into a free list element is to use the
first few bytes as a list structure.  In other
words, if you have a chunk of memory on the free list 
and that chunk
always has at least 12 bytes, then you can treat
the first twelve bytes of the memory as a list
structure.  In other words:

<UL>
<LI> bytes 0-3: Integer -- size of this chunk.
<LI> bytes 4-7: pointer to next chunk in the free list
<LI> bytes 8-11: pointer to previous chunk in the free list
</UL>

How do you do this?  You set up a typedef like the following:
<pre>
typedef struct flist {
   int size;
   struct flist *flink;
   struct flist *blink;
} *Flist;
</pre>
And when you need to treat a chunk of memory starting at location
<b>s</b> (where <b>s</b> is a <b>(char *)</b> or <b>(void *)</b> or 
<b>caddr_t</b>) as a free list element, you cast it:
<pre>
  Flist f;
  
  f = (Flist) s;
</pre>

<hr>
<h3>A Detailed Example</h3>

Now, we're going to run through an example, of running the program
<b><a href=program.c>program.c</a></b>.  I put the whole program below, but
we're going to show it working in bits and pieces.

<p><center><table border=3 cellpadding=3><td><pre>
#include &lt;stdio.h&gt;
#include &lt;stdlib.h&gt;

int main()
{
  int *p;
  unsigned char *q;
  int *r;
  int i;

  p = (int *)</font> malloc(sizeof(int) * 4);      <font color=blue>// Allocate 16 bytes for p and fill some of them in</font>
  p[0] = 678;
  p[1] = 0x6824abcd;
  p[3] = 5555;

  q = (unsigned char *)</font> malloc(sizeof(unsigned char) * 6);   <font color=blue>// Now 6 bytes for q</font>
  q[2] = 0x23;
  q[7] = 0xfe;              <font color=blue>// This writes past the "allocated" region but doesn't harm anything</font>

  free(p);                  <font color=blue>// free p to see its memory chunk go onto the free list</font>

  p = (int *)</font> malloc(sizeof(int) * 6);      <font color=blue>// Allocate 24 bytes and fill them in</font>
  for (i = 0; i &lt; 6; i++) p[i] = 100+i;

  r = (int *)</font> malloc(sizeof(int) * 2);      <font color=blue>// Allocate 8 bytes and fill in 12 bytes past what's allocated</font>
  for (i = 0; i &lt; 5; i++) r[i] = 200+i;
  
  q[8] = 127;                               <font color=blue>// This is going to damage the next allocated chunk</font>
  return 0;
}
</pre></td></table></center><p>


Heap memory starts as follows: <b>malloc_head</b> equals <b>NULL</b>.  
In other words, the free list is empty.  We run the first <b>malloc()</b> in the program:

<p><center><table border=3 cellpadding=3><td><pre>
  p = (int *)</font> malloc(sizeof(int) * 4);  
</pre></td></table></center><p>

Your <b>malloc</b> program sees that there is no memory
on the free list, so it calls <b>sbrk(8192)</b> to get
8K of heap storage for free memory.  Suppose
<b>sbrk(8192)</b> returns <b>0x6100</b>.  Then you have the
following view of memory: 

<pre>
          malloc_head == NULL

         |---------------|
         |               | 0x6100 (start of heap)
         |               | 0x6104
         |               | 0x6108
         |               | 0x610c
         |               | 0x6120
         |               |
               .....        
         |               |
         |---------------| 0x8100 (end of heap -- sbrk(0))
</pre>

To put this hunk of memory on the free list, you typecast it to an <b>Flist</b>, 
and then do the work to link it onto the list. 
At the end of this process, memory will look as follows (in this and the remaining
pictures, I'm going to color memory that has been changed to blue):

<pre>
          malloc_head == <font color=blue>0x6100</font>

         |---------------|
         |    <font color=blue>8192</font>       | 0x6100 (start of heap)
         |    <font color=blue>NULL</font>       | 0x6104
         |    <font color=blue>NULL</font>       | 0x6108
         |               | 0x610c
         |               | 0x6110
         |               |
               .....        
         |               |
         |---------------| 0x8100 (end of heap -- sbrk(0))
</pre>

You need to satisfy the user's request for 16 bytes.  You split this
chunk of memory into two chunks -- one that is 24 bytes (16 for the user
and 8 for bookkeeping), and one that is the remaining 8192-24 = 8168 bytes.
You put the latter chunk onto the free list, and return a pointer to the
16 bytes allocated for the user to the user:

<pre>
          malloc_head == <font color=blue>0x6118</font>

         |---------------|
         |      <font color=blue>24</font>       | 0x6100 (start of heap)
         |     <font color=blue>NULL</font>      | 0x6104
         |     <font color=blue>NULL</font>      | 0x6108  <-- beginning of 16 bytes for the user
         |               | 0x610c
         |               | 0x6110
         |               | 0x6114
         |     <font color=blue>8168</font>      | 0x6118
         |     <font color=blue>NULL</font>      | 0x611c
         |     <font color=blue>NULL</font>      | 0x6120
               .....        
         |               |
         |---------------| 0x8100 (end of heap -- sbrk(0))
</pre>

So after the <b>malloc()</b> call, <b>p</b> is set to 0x6108.  You'll note, I left
the NULL's in memory at 0x6104 and 0x6108.  That's because they are typically left
there.  Sometimes that can help you debug.

The next few lines set some
of <b>p's</b> elements:

<p><center><table border=3 cellpadding=3><td><pre>
  p[0] = 678;
  p[1] = 0x6824abcd;
  p[3] = 5555;
</pre></td></table></center><p>

Here's memory:
<pre>
          malloc_head == 0x6118

         |---------------|
         |      24       | 0x6100 (start of heap)
         |     NULL      | 0x6104
       p |     <font color=blue>678</font>       | 0x6108  
         |   <font color=blue>0x6824abcd</font>  | 0x610c
         |               | 0x6110
         |     <font color=blue>5555</font>      | 0x6114
         |     8168      | 0x6118
         |     NULL      | 0x611c
         |     NULL      | 0x6120
               .....        
         |               |
         |---------------| 0x8100 (end of heap -- sbrk(0))
</pre>

Now, in the next piece of code, the 
user calls <b>malloc(6)</b>.

<p><center><table border=3 cellpadding=3><td><pre>
  q = (unsigned char *) malloc(sizeof(unsigned char) * 6);
</pre></td></table></center><p>

You do the same thing -- carve 16
bytes off of the chunk of memory in the free list, and return a pointer
to 8 of them as the return value for <b>malloc</b>.  The other 8 bytes are for bookkeeping.  The
remaining 8152 bytes are put back on the free list.  In other words,
after <b>malloc(6)</b> is called, the heap looks like:

<pre>
          malloc_head == <font color=blue>0x6128</font>
         |---------------|
         |      24       | 0x6100 (start of heap)
         |     NULL      | 0x6104
       p |     678       | 0x6108
         |   0x6824abcd  | 0x610c
         |               | 0x6110
         |     5555      | 0x6114
         |      <font color=blue>16</font>       | 0x6118
         |     <font color=blue>NULL</font>      | 0x611c
       q |     <font color=blue>NULL</font>      | 0x6120 <----------- This is returned from malloc
         |               | 0x6124
         |     <font color=blue>8152</font>      | 0x6128
         |     <font color=blue>NULL</font>      | 0x612c
         |     <font color=blue>NULL</font>      | 0x6130
               .....        
         |               |
         |---------------| 0x8100 (end of heap -- sbrk(0))
</pre>

I left in the two "NULL's" at 0x611c and 0x6120, because they were part of the free node
before the 16 bytes were carved off, and they aren't overwritten.    
<p>
Next, we set two bytes:

<p><center><table border=3 cellpadding=3><td><pre>
  q[2] = 0x23;
  q[7] = 0xfe;              <font color=blue>// This writes past the "allocated" region but doesn't harm anything</font>
</pre></td></table></center><p>

Let's see the effect on memory:

<pre>
          malloc_head == 0x6128
         |---------------|
         |      24       | 0x6100 (start of heap)
         |     NULL      | 0x6104
       p |     678       | 0x6108
         |   0x6824abcd  | 0x610c
         |               | 0x6110
         |     5555      | 0x6114
         |      16       | 0x6118
         |     NULL      | 0x611c
       q |  <font color=blue> 0x00230000</font>  | 0x6120 
         |  <font color=blue> 0xfe??????</font>  | 0x6124
         |     8152      | 0x6128
         |     NULL      | 0x612c
         |     NULL      | 0x6130
               .....        
         |               |
         |---------------| 0x8100 (end of heap -- sbrk(0))
</pre>

Remember that NULL is 0x00000000.  So when we set <b>q[2]</b> to 0x23, that will set byte 2
to 0x23, but the others will remain at zero.  Why 0x00230000 rather than 0x00002300?  Because
our machines are little endian.
<p>
When we set <b>q[7]</b> to 0xfe, there are two things to notice.  First, I put question
marks for the other three bytes in the word, because we don't know what they were initially.
Second, no harm was done with our misuse of memory -- we allocated 6 bytes, but we received
8 bytes.  So when we set <b>q[7]</b>, we're lucky and nothing is wrong.  You shouldn't rely
on this -- who knows when they'll change malloc!
<p>
The next thing we do is:
<p><center><table border=3 cellpadding=3><td><pre>
  free(p);                  <font color=blue>// free p to see its memory chunk go onto the free list</font>
</pre></td></table></center><p>

Remember, <b>p</b> is 0x6108, so we will back up eight bytes to see the size of the
chunk: 24 bytes.  We need to put this 24-byte chunk onto the free list.  We'll put
it on the front, since that's nice and easy.  So, we'll:
<UL>
<LI> Set <b>malloc_head</b> to 0x6100.
<LI> Set the chunk's <b>flink</b>, which is at 0x6104, to the old head of the free-list: 0x6128.
<LI> Set the chunk's <b>blink</b> to be NULL.
<LI> Set the <b>blink</b> field of the old head of the free list to be 0x6100.
</UL>

Here's what memory looks like:

<pre>
          malloc_head == <font color=blue>0x6100</font>
         |---------------|
         |      24       | 0x6100 (start of heap)
         |    <font color=blue>0x6128</font>     | 0x6104
         |     <font color=blue>NULL</font>      | 0x6108
         |   0x6824abcd  | 0x610c
         |               | 0x6110
         |     5555      | 0x6114
         |      16       | 0x6118
         |     NULL      | 0x611c
       q |   0x00230000  | 0x6120 
         |   0xfe??????  | 0x6124
         |     8152      | 0x6128
         |     NULL      | 0x612c
         |    <font color=blue>0x6100</font>     | 0x6130
               .....        
         |               |
         |---------------| 0x8100 (end of heap -- sbrk(0))
</pre>


Next, we allocate <b>p</b> again, this time requesting 24 bytes:

<p><center><table border=3 cellpadding=3><td><pre>
  p = (int *) malloc(sizeof(int) * 6);  
</pre></td></table></center><p>

We need a chunk with 32 bytes, so we have to carve 32 bytes off the big 8152-byte
chunk.  That will leave a chunk with 8120 bytes as the second node of the free
list.  As before, I'll color the changed values of memory in blue.

<pre>
          malloc_head == 0x6100
         |---------------|
         |      24       | 0x6100 (start of heap)
         |    <font color=blue>0x6148</font>     | 0x6104
         |     NULL      | 0x6108
         |   0x6824abcd  | 0x610c
         |               | 0x6110
         |     5555      | 0x6114
         |      16       | 0x6118
         |     NULL      | 0x611c
       q |   0x00230000  | 0x6120 
         |   0xfe??????  | 0x6124
         |      <font color=blue>32</font>       | 0x6128
         |     NULL      | 0x612c
       p |    0x6100     | 0x6130 <------ Value returned from malloc(24)
         |               | 0x6134
         |               | 0x6138
         |               | 0x613c
         |               | 0x6140
         |               | 0x6144
         |     <font color=blue>8120</font>      | 0x6148
         |     <font color=blue>NULL</font>      | 0x614c
         |    <font color=blue>0x6100</font>     | 0x6150
               .....        
         |               |
         |---------------| 0x8100 (end of heap -- sbrk(0))
</pre>
The next chunk of code simply writes those 24 bytes:

<p><center><table border=3 cellpadding=3><td><pre>
  for (i = 0; i &lt; 6; i++) p[i] = 100+i;  
</pre></td></table></center><p>

Here's memory:

<pre>
          malloc_head == 0x6100
         |---------------|
         |      24       | 0x6100 (start of heap)
         |    0x6148     | 0x6104
         |     NULL      | 0x6108
         |   0x6824abcd  | 0x610c
         |               | 0x6110
         |     5555      | 0x6114
         |      16       | 0x6118
         |     NULL      | 0x611c
       q |   0x00230000  | 0x6120 
         |   0xfe??????  | 0x6124
         |      32       | 0x6128
         |     NULL      | 0x612c
       p |     <font color=blue>100</font>       | 0x6130 
         |     <font color=blue>101</font>       | 0x6134
         |     <font color=blue>102</font>       | 0x6138
         |     <font color=blue>103</font>       | 0x613c
         |     <font color=blue>104</font>       | 0x6140
         |     <font color=blue>105</font>       | 0x6144
         |     8120      | 0x6148
         |     NULL      | 0x614c
         |    0x6100     | 0x6150
               .....        
         |               |
         |---------------| 0x8100 (end of heap -- sbrk(0))
</pre>

Our last chunk of code allocates 8 bytes:

<p><center><table border=3 cellpadding=3><td><pre>
  r = (int *) malloc(sizeof(int) * 2);  
</pre></td></table></center><p>

We need 16 bytes for this, and we can get it from the first
node on the free list, since it has 24 bytes.
However there is a question --
should we carve off 16 bytes from it, or just use
all 24?  The answer is that we should use all 24
bytes (give the user 16 bytes even though he or she will
only think it's 8).  Why?  Because if we carve
off 16 bytes, we'll only have 8 leftover, and
that's not enough to store the pointers to hook
it into the free list.  Thus, we take the entire
24 bytes off the free list and give it to the
user: 

<pre>
          malloc_head == <font color=blue>0x6148</font>
         |---------------|
         |      24       | 0x6100 (start of heap)
         |    0x6148     | 0x6104
       r |     NULL      | 0x6108 <------------ Return value of malloc(8)
         |   0x6824abcd  | 0x610c
         |               | 0x6110
         |     5555      | 0x6114
         |      16       | 0x6118
         |     NULL      | 0x611c
       q |   0x00230000  | 0x6120 
         |   0xfe??????  | 0x6124
         |      32       | 0x6128
         |     NULL      | 0x612c
       p |     100       | 0x6130 
         |     101       | 0x6134
         |     102       | 0x6138
         |     103       | 0x613c
         |     104       | 0x6140
         |     105       | 0x6144
         |     8120      | 0x6148
         |     NULL      | 0x614c
         |     <font color=blue>NULL</font>      | 0x6150
               .....        
         |               |
         |---------------| 0x8100 (end of heap -- sbrk(0))
</pre>


Note how <b>malloc_head</b> and the
pointers in the chunk starting at <tt>0x6148</tt> are 
changed to reflect the
chunk being removed from the free list.

<p>
The last few lines of code are really bad, because they write over bytes that
aren't allocated for <b>r</b> and <b>q</b>.  


<p><center><table border=3 cellpadding=3><td><pre>
  for (i = 0; i &lt; 5; i++) r[i] = 200+i;  
  
  q[8] = 127;
</pre></td></table></center><p>

Let's see what happens:

<pre>
          malloc_head == 0x6148
         |---------------|
         |      24       | 0x6100 (start of heap)
         |    0x6148     | 0x6104
       r |     <font color=blue>200</font>       | 0x6108
         |     <font color=blue>201</font>       | 0x610c
         |     <b><font color=darkgreen>202</font></b>       | 0x6110
         |     <b><font color=darkgreen>203</font></b>       | 0x6114
         |     <b><font color=red>204</font></b>       | 0x6118  -- This is the "size" of q's chunk
         |     NULL      | 0x611c
       q |   0x00230000  | 0x6120 
         |   0xfe??????  | 0x6124
         |     <b><font color=red>127</font></b>       | 0x6128  -- This is the "size" of p's chunk
         |     NULL      | 0x612c
       p |     100       | 0x6130 
         |     101       | 0x6134
         |     102       | 0x6138
         |     103       | 0x613c
         |     104       | 0x6140
         |     105       | 0x6144
         |     8120      | 0x6148
         |     NULL      | 0x614c
         |     NULL      | 0x6150
               .....        
         |               |
         |---------------| 0x8100 (end of heap -- sbrk(0))
</pre>

I've colored 8 of the bytes green -- these are bytes that the user shouldn't have written,
but since he/she was allocated 16 bytes instead of 8, it doesn't hurt.  I've colored
8 of the bytes red, as these are disaster mistakes.  In each case, the user has overwritten
the size field of the next chunk in memory.  Think about what happens next if <b>q</b>
or <b>p</b> are freed?  Disaster.  

<hr>
<h3>More on free()</h3>

<b>Free()</b> must be called with addresses that have
been returned from <b>malloc()</b>.  
Why?  Because free expects that eight
bytes behind the address will contain the size of
the memory to be freed.  If you call free with an
address that wasn't allocated with <b>malloc()</b>, then
<b>free()</b> may well do something strange. 

<p>For example, look in 
<a href=badfree.c><b>badfree.c</b></a>.
It calls <b>free()</b>
with an address in the middle of an array, and
after a while, this causes <b>malloc()</b> to dump core.  It
takes a while since <b>malloc()</b> seems to allocate from
its buffer first, before attempting to use the
(bogus) free'd blocks. 

<p>In class it was suggested that <b>malloc()</b> keep track
of the addresses that it allocates, and <b>free()</b> should
check its argument to see whether or not it is a
valid one.  This is a good suggestion.  However,
<b>malloc()</b> will have to keep track of these addresses in a
red-black tree structure or a hash table to make
lookup fast.  Otherwise, <b>free()</b> will take too
long.  You should note that red-black trees as
implemented in <b>libfdr.a</b> cannot be used for this
purpose, because the jrb routines call <b>malloc()</b> and
<b>free()</b>. 

<p>Another way for <b>free()</b> to do error checking is to
use the bookkeeping bytes to help you.  You'll
notice that the word 4 bytes before the address
returned by <b>malloc()</b> is unused.  What you can do is
set that word to a checksum when you call <b>malloc()</b>.
Then when <b>free()</b> is called, it will check that word
to see if it has the desired checksum.  If not,
it will know that <b>free()</b> is being called with a
bad address and can flag the error. 

<hr>
<h3>Coalescing free blocks</h3>

When you call <b>free()</b>, you put a chunk of memory back 
on the free list.  There may be times when the chunk immediately
before it in memory, and/or the chunk immediately after it in 
memory are also free.  If so, it makes sense to try to merge
the free chunks into one free chunk, rather than having three
continguous free chunks on the free list.  This is called ``coalescing''
free chunks.

<p>Here is one way that you can perform coalescing.  (You 
should not do this in your lab).  However, if you want to give it a try
on your own, it is
a very good exercise in hacking.  Instead of having all 8 of your 
bookkeeping bytes before the memory allocated for the user, keep 4
before and 4 after.  When a memory chunk is allocated, both words
are set to be the size of the chunk.  

<p>Free chunks on the free list have a different layout.  They must
be at least 16 bytes in size.  The first 12 bytes should be as
described above, except <b>(-size)</b> should be kept in the first
four bytes instead of <b>size</b>.  The last 4 bytes of the chunk
should also hold <b>-size</b>.  

<p>What this lets you do is the following.  When you free a chunk,
you can look 8 bytes behind the pointer passed to <b>free()</b>.
That should be either:

<UL>
<LI> The <b>size</b> of the chunk before the given one in memory, if
     that chunk is allocated.
<LI> <b>-size</b> of the chunk before the given one in memory, if that
     chunk is free.  
</UL>

In other words, if the word 8 bytes before the given pointer is positive,
then the chunk before the one being freed has been allocated.  Otherwise,
it is free, and can be coalesced.  If the word after the current chunk
is negative, then the subsequent chunk is free and it can be coalesced.
Otherwise, it has been allocated.  Cool, no?

<p>
Obviously, you need to take care at the beginning and end of the 
heap to make sure that your checks all work.  This is not difficult.

