/* This program statically declares to arrays of characters:
    - A global variable s1, with 15 characters.
    - A local variable s2, with 4 characters.
    - It then tries to set s2 to "Jim", which will fail, because
      you can't copy arrays in C like you can in C++. */

#include <stdio.h>
#include <stdlib.h>

char s1[15];

int main(int argc, char **argv)
{
  char s2[4];
  
  s2 = "Jim";         // This line will not compile.
  return 0;
}
