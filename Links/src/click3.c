#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "fields.h"
#include "jrb.h"

int compare(Jval v1, Jval v2)
{
  return strcmp(v1.s+1, v2.s+1);
}

int main()
{
  IS is;
  JRB t;

  is = new_inputstruct(NULL);
  t = make_jrb();
 
  while (get_line(is) > 0) {
    jrb_insert_gen(t, new_jval_s(strdup(is->text1)), new_jval_i(0), compare);
  }

  printf("%s", t->blink->key.s);
  return 0;
}
