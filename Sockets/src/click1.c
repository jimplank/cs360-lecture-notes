#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

int main()
{
  FILE *f;
  int i, j;

  f = fopen("f4.txt", "r");
  fscanf(f, "%d", &i);

  if (fork() != 0) sleep(1);
  fscanf(f, "%d", &j);
  printf("%d %d\n", i, j);
  return 1;
}
