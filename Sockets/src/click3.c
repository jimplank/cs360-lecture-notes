#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

int main()
{
  int i;
  int fd, fd2;
  int status;

  fd = open ("f1.txt", O_WRONLY | O_CREAT | O_TRUNC, 0666);

  for (i = 0; i < 5; i++) {
    if (fork() == 0) {
      fd2 = open ("f2.txt", O_WRONLY | O_CREAT | O_TRUNC, 0666);
      write(fd2, "Binky\n", 6);
      i = 10;
    }
  }

  write(fd, "Fred\n", 5);
  
  close(fd);
  close(fd2);
  return 0;
}
